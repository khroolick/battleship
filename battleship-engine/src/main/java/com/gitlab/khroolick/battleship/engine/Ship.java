package com.gitlab.khroolick.battleship.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Ship {
	private List<Location> healthySegments;
	
	public Ship(Collection<Location> segments) {
		healthySegments = new ArrayList<Location>(segments);
	}
	
	public ShotResult applyShot(Location target) {
		boolean removed = healthySegments.remove(target);
		if (!removed) {
			return ShotResult.MISS;
		}
		
		if (healthySegments.isEmpty()){
			return ShotResult.SINK;
		} else {
			return ShotResult.HIT;
		}
	}

	@Override
	public String toString() {
		return "Ship [healthySegments=" + healthySegments + "]";
	}
}
