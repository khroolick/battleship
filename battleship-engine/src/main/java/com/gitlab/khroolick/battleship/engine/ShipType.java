package com.gitlab.khroolick.battleship.engine;

public enum ShipType {
	BATTLESHIP(5),
	DESTROYER(4);
	
	private final int size;

	private ShipType(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}
}
