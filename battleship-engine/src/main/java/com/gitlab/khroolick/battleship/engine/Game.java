package com.gitlab.khroolick.battleship.engine;

public class Game {
	final private Board board;
	final private ShipType[] shipsToDestroy;
	
	private int sinkCount;
	
	public Game(ShipType... shipsToDestroy) {
		this.shipsToDestroy = shipsToDestroy;
		board = new Board(shipsToDestroy);
	}

	public ShotResult shotInto(Location target) {
		final ShotResult result = board.applyShot(target);
		if (result == ShotResult.SINK) {
			sinkCount++;
		}
		return result;
	}
	
	public boolean isFinished() {
		return sinkCount == shipsToDestroy.length;
	}
	
	public static Game createDefault(){
		return new Game(
				ShipType.BATTLESHIP,
				ShipType.DESTROYER,
				ShipType.DESTROYER);
	}
}
