package com.gitlab.khroolick.battleship.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Board {
	protected static final int BOARD_SIZE = 10;
	private static final int MAX_ITERATIONS = 100;
	
	private final List<Ship> ships;

	public Board(ShipType... shipsToArrange) {
		this.ships = new ArrayList<Ship>(shipsToArrange.length);

		arrangeShips(shipsToArrange);
	}

	private void arrangeShips(ShipType[] shipsToArrange) {
		Random random = new Random();
		List<Location> occupiedSquares = new LinkedList<Location>();

		for (int iterationNo = 0; ships.size() < shipsToArrange.length; iterationNo++) {
			if (iterationNo == MAX_ITERATIONS) {
				throw new IllegalArgumentException(
						"Can't arrange: " + shipsToArrange + " ships on board with size: " + BOARD_SIZE);
			}

			ShipType type = shipsToArrange[ships.size()];
			List<Location> curentShipSegments = getRandomShipPosition(type, random);

			//check for collisions
			if (Collections.disjoint(curentShipSegments, occupiedSquares)) {
				ships.add(new Ship(curentShipSegments));
				occupiedSquares.addAll(curentShipSegments);
			}			
		}
	}

	private List<Location> getRandomShipPosition(ShipType currentShipType, Random rnd) {
		List<Location> shipSegments = new ArrayList<Location>(currentShipType.getSize());
		
		Direction direction = Direction.values()[rnd.nextInt(2)];
		int acrossStart = rnd.nextInt(BOARD_SIZE);
		int alongStart = rnd.nextInt(BOARD_SIZE - currentShipType.getSize() + 1);
		Location startLocation = createStartLocation(direction, acrossStart, alongStart);

		Location currentLocation = startLocation;
		for (int segmentId = 0; segmentId < currentShipType.getSize(); segmentId++) {
			shipSegments.add(currentLocation);
			currentLocation = currentLocation.getNext(direction);
		}
		
		return shipSegments;
	}

	private Location createStartLocation(Direction direction, int acrossStart, int alongStart) {
		if (direction == Direction.HORIZONTAL) {
			return new Location(alongStart, acrossStart);
		} else {
			return new Location(acrossStart, alongStart);
		}
	}

	public ShotResult applyShot(Location target) {
		for (Ship ship: ships) {
			ShotResult shipResult = ship.applyShot(target);
			if (shipResult != ShotResult.MISS) {
				return shipResult;
			}
		}
		
		return ShotResult.MISS;
	}

	@Override
	public String toString() {
		return "Board [ships=" + ships + "]";
	}
}
