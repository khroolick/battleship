package com.gitlab.khroolick.battleship.engine;

public enum Direction {
	HORIZONTAL,
	VERTICAL
}
