package com.gitlab.khroolick.battleship.engine;

public enum ShotResult {
	MISS,
	HIT,
	SINK
}
