package com.gitlab.khroolick.battleship.engine;

public class Location implements Comparable<Location> {
	final byte column;
	final byte row;
	
	public Location(int column, int row) {
		this.column = (byte) column;
		this.row = (byte) row;
	}

	public int compareTo(Location anotherLocation) {
		int _ret = Byte.compare(column, anotherLocation.column);
		
		if (_ret == 0){
			_ret = Byte.compare(row, anotherLocation.row);
		}
		
		return _ret;
	}

	@Override
	public String toString() {
		return "Location [colunm=" + column + ", row=" + row + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Location)) {
			return false;
		}
		Location other = (Location) obj;
		if (column != other.column) {
			return false;
		}
		if (row != other.row) {
			return false;
		}
		return true;
	}
	
	public Location getNext(Direction direction){
		switch (direction) {
		case HORIZONTAL:
			return new Location(column + 1, row);
		case VERTICAL:
			return new Location(column, row + 1);
		default:
			throw new IllegalArgumentException("unsupported direction: " + direction);
		}
	}
	
	public static Location valueOf(String text) {
		String normalized = text.trim().toUpperCase();
		return new Location(normalized.charAt(0) - 'A', Byte.valueOf(normalized.substring(1)) - 1);
	}
}
