package com.gitlab.khroolick.battleship.engine;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

@RunWith(Parameterized.class)
public class BoardShipArrangementTest {
	
	private ShipType[] shipsToArrange;

	@Parameters
	public static Object[][] data() {
	    return new Object[][] {
	    	{new ShipType[] {}},
	        {new ShipType[] {ShipType.BATTLESHIP }},
	        {new ShipType[] {ShipType.DESTROYER}},
	        {new ShipType[] {ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.DESTROYER, ShipType.DESTROYER}},
    		{new ShipType[] {ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP}},
    		{new ShipType[] {ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.DESTROYER}},
    		{new ShipType[] {ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.BATTLESHIP, ShipType.DESTROYER, ShipType.DESTROYER, ShipType.DESTROYER, ShipType.DESTROYER}}
	      };
	}

	public BoardShipArrangementTest(ShipType[] shipsToArrange) {
		this.shipsToArrange = shipsToArrange;
	}

	@Test
	public void properShipArrangementTest() {
		Multiset<ShotResult> expectedResults = calculateExpectedResults();
		
		Board board = new Board(shipsToArrange);
		Multiset<ShotResult> results = gatherResults(board);
		
		assertThat(results, is(expectedResults));
	}

	private Multiset<ShotResult> gatherResults(Board board) {
		Multiset<ShotResult> results = HashMultiset.create(ShotResult.values().length);
		//get results for all board
		for (int col = 0; col < Board.BOARD_SIZE; col++){
			for (int row = 0; row < Board.BOARD_SIZE; row++){
				ShotResult shotResult = board.applyShot(new Location(col, row));
				results.add(shotResult);
			}
		}
		return results;
	}

	private Multiset<ShotResult> calculateExpectedResults() {
		int expectedSinks = shipsToArrange.length;
		int expectedHits = Arrays.asList(shipsToArrange).stream().collect(
				Collectors.summingInt(ship -> ship.getSize() - 1)
			);
		int expectedMiss = Board.BOARD_SIZE * Board.BOARD_SIZE - expectedHits - expectedSinks; 
		
		Multiset<ShotResult> expectedResults = HashMultiset.create(ShotResult.values().length);
		expectedResults.add(ShotResult.SINK, expectedSinks);
		expectedResults.add(ShotResult.HIT, expectedHits);
		expectedResults.add(ShotResult.MISS, expectedMiss);
		return expectedResults;
	}

}
