package com.gitlab.khroolick.battleship.engine;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ShipTest {
	Ship ship;
	List<Location> shipSegments = Arrays.asList(new Location[]{
			new Location(1,1),
			new Location(1,2),
	});
	
	@Before
	public void setUp() throws Exception {
		ship = new Ship(shipSegments);
	}

	@Test
	public void testApplyAccurateShotSequence() {
		for (Iterator<Location> iterator = shipSegments.iterator(); iterator.hasNext();) {
			ShotResult accurateShotResult = ship.applyShot(iterator.next());
			
			if (iterator.hasNext()){
				assertThat(accurateShotResult, is(ShotResult.HIT));
			} else {
				assertThat("The last accurate shot should be deadly", accurateShotResult, is(ShotResult.SINK));
			}
		}
	}

	@Test
	public void testRetryShotToHitAgain(){
		Location target = new Location(1, 2);
		ShotResult firstShotResult = ship.applyShot(target);
		ShotResult secondShotResult = ship.applyShot(target);
		
		assertThat(firstShotResult, is(ShotResult.HIT));
		assertThat("You can't double score same ship segment.", secondShotResult, is(ShotResult.MISS));
	}
	
	@Test
	public void testApplySomeBlindShots(){
		//north 
		assertThat(ship.applyShot(new Location(0, 0)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(0, 1)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(0, 2)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(0, 3)), is(ShotResult.MISS));
		
		//same row
		assertThat(ship.applyShot(new Location(1, 0)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(1, 3)), is(ShotResult.MISS));
		
		//south
		assertThat(ship.applyShot(new Location(2, 0)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(2, 1)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(2, 2)), is(ShotResult.MISS));
		assertThat(ship.applyShot(new Location(2, 3)), is(ShotResult.MISS));
	}

}
