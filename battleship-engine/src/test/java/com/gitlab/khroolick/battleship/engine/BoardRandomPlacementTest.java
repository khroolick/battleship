package com.gitlab.khroolick.battleship.engine;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

public class BoardRandomPlacementTest {
	
	private static final int ITERATIONS_QTY = 10000;

	private ShipType[] shipsToArrange = new ShipType[]{
			ShipType.BATTLESHIP,
			ShipType.DESTROYER,
			ShipType.DESTROYER};

	static Set<Location> ALL_LOCATIONS = Collections.unmodifiableSet(getAllBoardLocations());

	@Test
	public void properRandomArrangementTest() {
		Multiset<Location> locations = HashMultiset.create(ALL_LOCATIONS.size());
		for (int it = 0; it < ITERATIONS_QTY; it++){
			Board board = new Board(shipsToArrange);
			Set<Location> currentLocations = gatherLocations(board, EnumSet.of(ShotResult.HIT, ShotResult.SINK));
			locations.addAll(currentLocations);
		}
		
		//
		//if ship arrangement is random and distributed all over the board
		//we should get all board locations after a dozen iterations
		assertThat("Ships should be located randomly all over the board", locations.elementSet(), is(equalTo(ALL_LOCATIONS)));
		
		
		//
		// TEST IS OVER stuff for fun only
		//Check if we get evenly distributed locations
		//	we can have some hints where to shot first - a little bit of cheating. 
		//
		Location hottestLocation = Multisets.copyHighestCountFirst(locations).elementSet().iterator().next();
		int hottestCount = locations.count(hottestLocation);
		double hottestProbability = hottestCount/(double)ITERATIONS_QTY;
		
		System.out.println("Hit probability (linear scale)");
		for (int row = 0; row < Board.BOARD_SIZE; row++) {
			System.out.format("%2d|", row + 1);
			for (int col = 0; col < Board.BOARD_SIZE; col++) {
				Location loc = new Location(col, row);
				int score = (int) (locations.count(loc)/(double)hottestCount*10);
				if (score == 10){
					System.out.print('*');
				} else {
					System.out.print(score);
				}
				System.out.print(' ');
			}
			System.out.println();
		}
		System.out.println("   A B C D E F G H I J");
		
		System.out.println("\nLegend:");
		for (int scale = 0; scale < 10; scale++){
			System.out.format("%d - p(A) < %2.2f\n", scale, hottestProbability/10*(scale+1));
		}
		System.out.format("* - p(A) = %2.2f (max)\n\n", hottestProbability);
	}

	private static Set<Location> getAllBoardLocations() {
		Set<Location> allBoardLocations = new TreeSet<>();
		for (int col = 0; col < Board.BOARD_SIZE; col++){
			for (int row = 0; row < Board.BOARD_SIZE; row++){
				allBoardLocations.add(new Location(col, row));
			}
		}
		return allBoardLocations;
	}

	private Set<Location> gatherLocations(Board board, Set<ShotResult> results) {
		Set<Location> locations = new HashSet<>();
		//get results location for all board
		for (int col = 0; col < Board.BOARD_SIZE; col++){
			for (int row = 0; row < Board.BOARD_SIZE; row++){
				Location target = new Location(col, row);
				ShotResult shotResult = board.applyShot(target);
				if (results.contains(shotResult)){
					locations.add(target);
				}
			}
		}
		return locations;
	}
}
