package com.gitlab.khroolick.battleship.engine;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LocationTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCompareTo() {
		assertThat(new Location(1, 1), 
				allOf(
					greaterThan(new Location(0,9)),
					greaterThan(new Location(1,0)),
					greaterThan(new Location(0,1)),
					comparesEqualTo(new Location(1,1)),
					lessThan(new Location(1, 2)),
					lessThan(new Location(2, 1))
					));
	}

	@Test
	public void testGetNextHorizontal() {
		Location start = new Location(5, 5);
		Location expected = new Location(6, 5);
		
		Location next = start.getNext(Direction.HORIZONTAL);
		
		assertThat(expected, is(next));
	}
	
	@Test
	public void testGetNextVertical() {
		Location start = new Location(5, 5);
		Location expected = new Location(5, 6);
		
		Location next = start.getNext(Direction.VERTICAL);
		
		assertThat(expected, is(next));
	}

	//
	// value of
	//
	
	@Test
	public void testValueOf_1() {
		assertThat(Location.valueOf("a1"), is(new Location(0, 0)));
	}
	
	@Test
	public void testValueOf_lastCorner() {
		assertThat(Location.valueOf("j10"), is(new Location(9, 9)));
	}
	
	@Test
	public void testValueOf_big() {
		assertThat(Location.valueOf("z99"), is(new Location(25, 98)));
	}
	
	
	@Test (expected = RuntimeException.class)
	public void testValueOf_tooMuchCol() {
		Location.valueOf("ab4");
	}
	
	@Test (expected = RuntimeException.class)
	public void testValueOf_tooMuchRow() {
		Location.valueOf("a9999");
	}
	
	@Test (expected = RuntimeException.class)
	public void testValueOf_null() {
		Location.valueOf(null);
	}
	
	@Test (expected = RuntimeException.class)
	public void testValueOf_randomtext() {
		Location.valueOf("żabka");
	}
}
