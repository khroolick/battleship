package com.gitlab.khroolick.battleship.engine;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static com.gitlab.khroolick.battleship.engine.ShipType.*;

import org.junit.Test;

public class ShipTypeTest {
	@Test
	public void testBattleshipSize() {
		assertThat(BATTLESHIP, hasProperty("size", equalTo(5)));
	}
	
	@Test
	public void testDestroyerSize() {
		assertThat(DESTROYER, hasProperty("size", equalTo(4)));
	}
}
