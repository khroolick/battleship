package com.gitlab.khroolick.battleship.engine;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Game game = Game.createDefault();
		int sinkCount = 0;
		for (Location target : BoardRandomPlacementTest.ALL_LOCATIONS){
			ShotResult shotResult = game.shotInto(target);
			if (shotResult == ShotResult.SINK){
				sinkCount++;
			}
			assertThat(game.isFinished(), is(equalTo(sinkCount == 3)));
		}
		assertThat(game.isFinished(), is(true));
	}

}
