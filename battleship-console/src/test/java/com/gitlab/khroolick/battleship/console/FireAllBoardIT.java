package com.gitlab.khroolick.battleship.console;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FireAllBoardIT {
	private ProcessBuilder pb;
	Process game;
	
	@Before
	public void setUp() throws Exception {
		String jarPath = System.getProperty("it.artifact.jar");
		String inputFilePath = System.getProperty("it.input.file");
		pb = new ProcessBuilder("java", "-jar", jarPath)
				.redirectInput(new File(inputFilePath));
	}
	
	@After
	public void close() throws IOException, InterruptedException {
		game.destroyForcibly().waitFor();
	}

	@Test()
	public void test() throws IOException, InterruptedException {
		game = pb.start();
		new StreamYumYum(game.getInputStream()).start();
		new StreamYumYum(game.getErrorStream()).start();
		
		boolean terminated = game.waitFor(5, TimeUnit.SECONDS);
		assertThat("Game should be finished", terminated, is(true));
	}

	class StreamYumYum extends Thread {
		private InputStream in;
		
		public StreamYumYum(InputStream in) {
			this.in = in;
		}
		
		@Override
		public void run() {
			try {
				while(in.read()>0){
				}
			} catch (IOException e) {
				//ignore
			}
		}
	}
}
