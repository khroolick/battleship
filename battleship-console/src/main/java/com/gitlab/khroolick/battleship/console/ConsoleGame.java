package com.gitlab.khroolick.battleship.console;

import java.io.IOException;
import java.util.Scanner;

import com.gitlab.khroolick.battleship.engine.Game;
import com.gitlab.khroolick.battleship.engine.Location;
import com.gitlab.khroolick.battleship.engine.ShotResult;

public class ConsoleGame {
	private static final String MSG_CONGRATULATIONS = "Well done!";
	private static final String MSG_HELP = "Enter shot coordinates, e.g. A1";
	private static final String MSG_WELCOME_BANNER = "Welcome to the Battleships game!";
	private static final String PROMPT = "battleship>";
	
	private Game game = Game.createDefault();
	private Scanner input;
	
	public ConsoleGame() {
		input = new Scanner(System.in);
	}
	
	public void play() {
		System.out.println(MSG_WELCOME_BANNER);
		printHelp();
		while(!game.isFinished()) {
			try {
				Location target = getLocationFromPlayer();
				ShotResult shotResult = game.shotInto(target);
				System.out.println(shotResult);
			} catch (RuntimeException e){
				printHelp();
			}
		}
		System.out.println(MSG_CONGRATULATIONS);
	}

	void printHelp(){
		System.out.println(MSG_HELP);
	}
	
	private Location getLocationFromPlayer() {
		System.out.print(PROMPT);
		String userInput = input.nextLine();
		return Location.valueOf(userInput);
	}
	
	public static void main(String[] args) throws IOException {
		new ConsoleGame().play();
	}
}
